import os
from selenium import webdriver


# check for duplicate lines
lines = []
with open('ցանկ') as f:
    for line in f:
        line_s = line.strip()
        if line_s in lines:
            raise NameError('duplicate entry: ' + line_s)
        else:
            lines.append(line_s)

options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('window-size=1080,607')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

f2 = open('./public/index.html', 'a')
driver = webdriver.Chrome(options=options)
for line in lines:
    print('starting '+line)
    url = 'https://'+line+'.հայ'
    driver.get(url)
    s = '<div><p><a href="'+url+'">'+line+'</a> <span><a href="'+line+'"><img src="lh.svg" alt="փարոս"></a></span></p><a href="'+url+'"><img src="'+line+'.webp" alt="' +line+'.հայ"></a></div>\n'
    f2.write(s)
    os.system('lighthouse ' + url + ' --chrome-flags="--headless --no-sandbox --disable-dev-shm-usage" --output-path=stdout | tail -n +17 > ./public/'+line)
    os.system("sed -i 's/" + (line + ".հայ").encode('idna').decode('ascii') + "/" + line + ".հայ/g' ./public/" + line)
    os.system('gzip -k9 ./public/'+line)
    driver.save_screenshot(line+'.png')
    os.system('cwebp -z 9 ' + line + '.png -o ./public/' + line + '.webp')


f2.write('</main><footer><a href="https://gitlab.com/petrosyan1/pages"><img style="width:3em" src="https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg" alt="գիթլաբ"></a></footer></body></html>')
f2.close()
